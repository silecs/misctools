#!/bin/bash
# nécessite mysqldump, gzip, pv
source ./clonedb.conf

db_size=$(mysql \
    -u"$DBUSER" \
    -p"$DBPASS" \
    --silent \
    --skip-column-names \
    -e "SELECT ROUND(SUM(data_length) / 1024 / 1024, 0) \
        FROM information_schema.TABLES \
        WHERE table_schema='$DBNAME_PROD';")

targetsize=$(( db_size * zipratio / 100 ))

echo "Taille brute de $DBNAME_PROD : $db_size Mb";
echo "Taux estimé de compression : $ZIPRATIO %";
echo "Taille estimée sql.gz : $targetsize Mb\n";

time mysqldump -u$DBUSER -p$DBPASS $DBNAME_PROD \
    | pv --progress --size  "$db_size"m \
    | gzip  > ${DIR}/$DBNAME_PROD.sql.gz
filesize=$(find ${DIR}/$DBNAME_PROD.sql.gz -printf '%s') 
echo "Taille ${DIR}/$DBNAME_PROD.sql.gz : $filesize";

echo "DROP DATABASE $DBNAME_TEST" | mysql -u$DBUSER -p$DBPASS
echo "CREATE DATABASE $DBNAME_TEST CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci" \
    | mysql -u$DBUSER -p$DBPASS
time cat ${DIR}/$DBNAME_PROD.sql.gz \
    | pv --progress --size $filesize | zcat \
    | mysql -u$DBUSER -p$DBPASS $DBNAME_TEST

