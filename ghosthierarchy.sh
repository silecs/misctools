#!/bin/bash
#source https://superuser.com/questions/504644/rsync-only-filenames
# copie récursivement une hiérarchie du système de fichiers en remplaçant tous les fichiers par des fichiers vides

SOURCEDIR=$1 # original structure
TARGETDIR=$2  # where to create empty files
[[ -d $SOURCEDIR ]] || exit 1
[[ -d $TARGETDIR ]] || mkdir -p $TARGETDIR || exit 1

cd $SOURCEDIR
time find . -type d -exec mkdir -p $TARGETDIR/{} \;
time find . -type f -exec touch $TARGETDIR/{} \;

